#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <curl/curl.h>

// apt install libcurl4-openssl-dev
// gcc speed_test_onion.c -o speed_test_onion -lcurl

#define NUM_TESTS 2

struct MemoryStruct {
    char *memory;
    size_t size;
};

void init_memory_struct(struct MemoryStruct *chunk) {
    chunk->memory = (char *)malloc(1);
    chunk->size = 0;
}

size_t write_memory_callback(void *contents, size_t size, size_t nmemb, void *userp) {
    size_t realsize = size * nmemb;
    struct MemoryStruct *mem = (struct MemoryStruct *)userp;

    char *ptr = (char *)realloc(mem->memory, mem->size + realsize + 1);
    if (ptr == NULL) {
        // Out of memory
        fprintf(stderr, "Memory allocation failed\n");
        return 0;
    }

    mem->memory = ptr;
    memcpy(&(mem->memory[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->memory[mem->size] = 0;

    return realsize;
}

double measure_internet_speed(int num_tests) {
    const char *sites[NUM_TESTS] = {
        "https://duckduckgogg42xjoc72x3sjasowoarfbgcmvfimaftt6twagswzczad.onion",
        "https://invidious.flokinet.to/feed/popular"
    };

    if (num_tests > NUM_TESTS) {
        num_tests = NUM_TESTS;
    }

    double average_speed = 0.0;

    for (int i = 0; i < num_tests; i++) {
        CURL *curl_handle;
        CURLcode res;
        struct MemoryStruct chunk;
        chunk.memory = NULL;
        chunk.size = 0;

        curl_global_init(CURL_GLOBAL_ALL);
        curl_handle = curl_easy_init();

        if (curl_handle) {
            curl_easy_setopt(curl_handle, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36");

            // Defina o proxy SOCKS5 aqui
            curl_easy_setopt(curl_handle, CURLOPT_PROXY, "socks5h://127.0.0.1:9050");
            curl_easy_setopt(curl_handle, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5_HOSTNAME);

            curl_easy_setopt(curl_handle, CURLOPT_URL, sites[i]);
            curl_easy_setopt(curl_handle, CURLOPT_WRITEFUNCTION, write_memory_callback);
            curl_easy_setopt(curl_handle, CURLOPT_WRITEDATA, (void *)&chunk);

            res = curl_easy_perform(curl_handle);

            if (res == CURLE_OK) {
                double download_speed = chunk.size;
                printf("Download Speed for Site %d: %.0f bytes/s (%.0f kB/s)\n", i + 1, download_speed, download_speed / 1024.0);
                average_speed += download_speed;
            } else {
                fprintf(stderr, "Failed to establish a connection to %s: %s\n", sites[i], curl_easy_strerror(res));
            }

            curl_easy_cleanup(curl_handle);
            free(chunk.memory);
        }

        curl_global_cleanup();
    }

    if (num_tests > 0) {
        average_speed /= num_tests;
        printf("Average Download Speed: %.0f bytes/s (%.0f kB/s)\n", average_speed, average_speed / 1024.0);
    } else {
        printf("No internet speed measurements available.\n");
    }

    return average_speed;
}

int main(int argc, char *argv[]) {
    int precision = 1;

    if (argc == 2) {
        precision = atoi(argv[1]);
    }

    if (precision <= 0) {
        fprintf(stderr, "Invalid precision value\n");
        return 1;
    }

    double average_speed = measure_internet_speed(precision);

    return 0;
}
